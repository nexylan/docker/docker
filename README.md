# Docker

On steroids Docker image.

## Usage

```
docker run registry.gitlab.com/nexylan/docker/docker:x.y.z docker images
```

The usage is exactly the same as the [official docker image][docker_hub_image_docker].

## Configuration

This image can be pre-configured using the environment variables.

All the process is done thanks to the [entrypoint](rootfs/entrypoint.sh)

### Auto login

Provide the following variables to get automatically login on your registry.

Example:

```
❯ docker run -e DOCKER_HUB_USER="username" -e DOCKER_HUB_PASSWORD="secret_pass" nexylan/docker/docker ls
login: username@docker.io

Login Succeeded
```

You may provide multiple credentials at a time.

#### Docker HUB

- `DOCKER_HUB_USER`
- `DOCKER_HUB_PASSWORD`

#### GitLab CI registry

- `CI_REGISTRY_IMAGE`
- `CI_REGISTRY_USER`
- `CI_REGISTRY_PASSWORD`

Note: If you are running this image on a GitLab CI runner, [the variables will be automatically provided][gitlab_ci_predefined_variables].

### Base image

You can setup the base image tag and name to take benefit on some automated tools.

It fits automatically with a Gitlab CI runner.

- `IMAGE_NAME`
- `IMAGE_TAG`
- `IMAGE` (default to `${IMAGE_NAME}:${IMAGE_TAG}`)

[docker_hub_image_docker]: https://hub.docker.com/_/docker "Docker image"
[gitlab_ci_predefined_variables]: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html "Predefined CI variables"
